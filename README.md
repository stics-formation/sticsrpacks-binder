# RStudio on Binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fstics-formation%2Fsticsrpacks-binder.git/HEAD?urlpath=shiny/SticsRpacks/)

Cliquez sur le bouton [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fstics-formation%2Fsticsrpacks-binder.git/HEAD?urlpath=rstudio) pour lancer dans son navigateur web un environnement [Rstudio](https://posit.co/products/open-source/rstudio/) sur le service [mybinder.org](https://mybinder.org).

Utilise les dernières versions des paquets R de [SticsRPacks](https://sticsrpacks.github.io/SticsRPacks/) pour les formations au modèle [Stics](https://www6.paca.inrae.fr/stics/)

```
├── runtime.txt    # la version de R utilisée (au format `r-<RVERSION>-<YYYY>-<MM>-<DD>`)
├── apt.txt        # les paquets apt à installer
├── install.R      # les paquets R à installer
├── postBuild      # des commandes lancées après les installations apt et R
└── start          # un script lancé au demarrage   
```

 ***

## Releases:  
* [SticsRPacks@v0.4.0](https://github.com/SticsRPacks/SticsRPacks/releases/tag/v0.4.0): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fstics-formation%2Fsticsrpacks-binder.git/v0.4.0?urlpath=rstudio)
* [SticsRPacks@v0.6.2](https://github.com/SticsRPacks/SticsRPacks/releases/tag/v0.6.2): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fstics-formation%2Fsticsrpacks-binder.git/v0.6.2?urlpath=rstudio)
* [SticsRPacks@v0.7.1](https://github.com/SticsRPacks/SticsRPacks/releases/tag/v0.7.1): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fstics-formation%2Fsticsrpacks-binder.git/v0.7.1?urlpath=rstudio)


### Alternative en utilisant un conteneur local

Pré-requis : [docker](https://www.docker.com/get-started/)

Lancement d'un conteneur local via la commande 
```shell
docker run \
        --rm \
        -ti \
        --user root \
        -e DISABLE_AUTH=true \
        -p 127.0.0.1:8787:8787 \
        registry.forgemia.inra.fr/stics-formation/sticsrpacks-binder:v0.7.1
```
Puis ouvrir un navigateur avec l'url <http://127.0.0.1:8787/>  
Puis dans l'interface rstudio cliquer sur le bouton `Start Tutorial` de *Tutorial for SticsRPacks* dans l'onglet `Tutorial` (en haut à droite)

Pour arreter le serveur local faire `Ctrl + C` dans le terminal.  
